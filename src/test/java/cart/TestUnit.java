package cart;
import static org.junit.Assert.*;

import cart.Item;
import cart.Methodes;
import org.junit.Test;

import java.util.ArrayList;


public class TestUnit {

    public class CalculatorTest {

        @Test
        public final void testAdd() {

            ArrayList<Item> basket = new ArrayList<Item>(3);

            basket.add(new Item("Apple", 0.2, 2, 8));

            basket.add(new Item("Orange", 0.5, 0, 7));

            basket.add(new Item("Watermelon", 0.8, 3, 8));

            assertEquals(" error ",Methodes.calculeDuPanier(basket), 9.1, 1);
        }


    }


}
